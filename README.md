Readme
======
CSPP\_Syslog extends the CSPP_Core package of the **CS++** Project. 

It contains a derived class of the CSPP_MessageLogger. 

Refer to https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home for **CS++** project overview, details and documentation.

LabVIEW 2021 is the currently used development environment.

Related documents and information
---------------------------------
- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/CSPP_Syslog
- Documentation:
  - Refer to package folder
  - Project-Wiki: https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home
  - NI Actor Framework: https://ni.com/actorframework

GIT Submodules
--------------
This package can be used as submodule

- Packages\CSPP_Syslog: Implementation of CS++MessageLogger class using the Syslog library

External Dependencies
---------------------
- CSPP_Core: https://git.gsi.de/EE-LV/CSPP/CSPP_Core
- Syslog; Refer to http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116 or use the VIPM

Getting started:
=================================
- Add CSPP_Syslog.lvlib into your own LabVIEW project.
- Add CSPP_Syslog.lvclass into your desired case of the CSPP_UserContents.vi
- You need to extend your project specific ini-file.
  - A sample ini-file should be available on disk in the corresponding package folder.
- You need to start your central Syslog message logger somewhere.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2013  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: https://eupl.eu/

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.